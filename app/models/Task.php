<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    
    protected $fillable = ['description','start_date','end_date','title','created_by','updated_by',];
}
